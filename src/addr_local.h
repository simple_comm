/*******************************************************************************
********************************************************************************

        Copyright (c) 2008 Ahmed S. Badran

	Licensed under the FreeBSD License (see LICENSE)

          Filename:  addr_local.h
       Description:  Localhost address.
           Created:  12/26/2008 04:02:54 PM PST
            Author:  Ahmed S. Badran (Ahmed B.), ahmed.badran@gmail.com

********************************************************************************
*******************************************************************************/
#ifndef  ADDR_LOCAL_INC
#define  ADDR_LOCAL_INC
#include "addr_if.h"

namespace simple_comm
{

class addr_local: public addr_if
{
	public:
	addr_local();
	const std::string get() const;
	addr_if* clone() const;
	private:
	const std::string name;
};

}
#endif   // ----- #ifndef ADDR_LOCAL_INC  -----
