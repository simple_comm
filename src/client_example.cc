/*******************************************************************************
********************************************************************************

        Copyright (c) 2008 Ahmed S. Badran

	Licensed under the FreeBSD License (see LICENSE)

          Filename:  client_example.cc
       Description:  Sample client example.
           Created:  12/26/2008 08:00:01 PM PST
            Author:  Ahmed S. Badran (Ahmed B.), ahmed.badran@gmail.com

********************************************************************************
*******************************************************************************/
#include "client.h"
#include "port.h"
#include "addr_local.h"
#include <cstring>
using namespace simple_comm;

int main()
{
	port p(10000);
	addr_local server_addr;
	client cl(server_addr, p);
	conn c = cl.connect();
	u32 i = 0;
	const u32 MAX = 100;
	char buf[MAX];
	printf("connection id: %d\n", c.get_id());
	while (i < 10) {
		memset(buf, '\0', MAX * sizeof(char));
		sprintf(buf, "sent %d", i+1);
		cl.write(c, (u8*) buf, MAX);
		++i;
	}
	return 0;
}
