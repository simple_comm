/*******************************************************************************
********************************************************************************

        Copyright (c) 2008 Ahmed S. Badran

	Licensed under the FreeBSD License (see LICENSE)

          Filename:  port.h
       Description:  A port class.
           Created:  12/20/2008 07:01:52 PM PST
            Author:  Ahmed S. Badran (Ahmed B.), ahmed.badran@gmail.com

********************************************************************************
*******************************************************************************/
#ifndef  PORT_INC
#define  PORT_INC
#include "port_if.h"
namespace simple_comm
{

class port: public port_if
{
	public:
	port(u16 n);
	u16 get() const;
	private:
	u16 port_num;
};

}
#endif   // ----- #ifndef PORT_INC  -----
