/*******************************************************************************
********************************************************************************

        Copyright (c) 2008 Ahmed S. Badran

	Licensed under the FreeBSD License (see LICENSE)

          Filename:  client.cc
       Description:  Implementation.
           Created:  12/26/2008 07:08:30 PM PST
            Author:  Ahmed S. Badran (Ahmed B.), ahmed.badran@gmail.com

********************************************************************************
*******************************************************************************/
#include "client.h"
#include "addr_if.h"
#include "port.h"
#include "generic_exception.h"
#include "sock_inf.h"
#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>
#include <cerrno>
#include <cstring>
using namespace simple_comm;

client::client (const addr_if& a, const port_if& p):
	server_addr(0), server_port(0)
{
	server_addr = a.clone();
	server_port = new port(p.get());

	sock_info = new sock_inf;
	sock_info->fd = socket(AF_INET, SOCK_STREAM, 0);
	if (sock_info->fd == -1) {
		throw generic_exception(strerror(errno));
	}
	struct addrinfo hint;
	memset(&hint, '\0', sizeof(hint));
	hint.ai_family = AF_INET;
	hint.ai_socktype = SOCK_STREAM;
	char buf[10] = {0};
	sprintf(buf, "%d", server_port->get());
	int err = getaddrinfo(server_addr->get().c_str(), buf, &hint,
			      &(sock_info->addr_inf));
	if (err != 0) {
		throw generic_exception(gai_strerror(err));
	}
}

conn
client::connect ()
{
	int r = ::connect(sock_info->fd, sock_info->addr_inf->ai_addr,
			  sock_info->addr_inf->ai_addrlen);
	if (r == -1) {
		throw generic_exception(strerror(errno));
	} else {
		return conn(sock_info->fd);
	}
}

u32
client::read (const conn& con, u8* buf, u32 len) const
{
	return ::read(con.get_id(), buf, len);
}

u32
client::write (const conn& con, u8* buf, u32 len)
{
	return ::write(con.get_id(), buf, len);
}

client::~client ()
{
	close(sock_info->fd);
	freeaddrinfo(sock_info->addr_inf);
}

