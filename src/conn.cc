/*******************************************************************************
********************************************************************************

        Copyright (c) 2008 Ahmed S. Badran

	Licensed under the FreeBSD License (see LICENSE)

          Filename:  conn.cc
       Description:  Implementation.
           Created:  12/20/2008 08:22:33 PM PST
            Author:  Ahmed S. Badran (Ahmed B.), ahmed.badran@gmail.com

********************************************************************************
*******************************************************************************/
#include "conn.h"
using namespace simple_comm;

conn::conn (int n):
	id(n)
{
}

int
conn::get_id () const
{
	return id;
}

conn::~conn ()
{
}

