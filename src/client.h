/*******************************************************************************
********************************************************************************

        Copyright (c) 2008 Ahmed S. Badran

	Licensed under the FreeBSD License (see LICENSE)

          Filename:  client.h
       Description:  A client class.
           Created:  12/20/2008 03:22:59 PM PST
            Author:  Ahmed S. Badran (Ahmed B.), ahmed.badran@gmail.com

********************************************************************************
*******************************************************************************/
#ifndef  CLIENT_INC
#define  CLIENT_INC
#include "config.h"
#include "conn.h"
namespace simple_comm
{

class addr_if;
class port_if;
struct sock_inf;

class client
{
	public:
	client(const addr_if&, const port_if&);
	u32 read (const conn& con, u8* buf, u32 len) const;
	u32 write (const conn& con, u8* buf, u32 len);
	conn connect ();
	~client();
	private:
	const addr_if* server_addr;
	const port_if* server_port;
	sock_inf* sock_info;
};

}
#endif   // ----- #ifndef CLIENT_INC  -----
