/*******************************************************************************
********************************************************************************

        Copyright (c) 2008 Ahmed S. Badran

	Licensed under the FreeBSD License (see LICENSE)

          Filename:  addr_ip.cc
       Description:  Implementation.
           Created:  12/26/2008 04:40:59 PM PST
            Author:  Ahmed S. Badran (Ahmed B.), ahmed.badran@gmail.com

********************************************************************************
*******************************************************************************/
#include "addr_ip.h"
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
using namespace std;
using namespace simple_comm;

addr_ip::addr_ip (u8 a_, u8 b_, u8 c_, u8 d_):
	a(a_), b(b_), c(c_), d(d_)
{
	struct in_addr addr;
	addr.s_addr = (a << 24) | (b << 16) | (c << 8) | d;
	name = inet_ntoa(addr);
}

const string
addr_ip::get () const
{
	return name;
}

addr_if*
addr_ip::clone () const
{
	return new addr_ip(a, b, c, d);
}


