/*******************************************************************************
********************************************************************************

        Copyright (c) 2008 Ahmed S. Badran

	Licensed under the FreeBSD License (see LICENSE)

          Filename:  port.cc
       Description:  Implementation.
           Created:  12/20/2008 07:02:37 PM PST
            Author:  Ahmed S. Badran (Ahmed B.), ahmed.badran@gmail.com

********************************************************************************
*******************************************************************************/

#include "port.h"
#include <cstdio>
using namespace simple_comm;

port::port (u16 n)
	: port_num(n)
{
}

u16
port::get () const
{
	return port_num;
}
