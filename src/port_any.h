/*******************************************************************************
********************************************************************************

        Copyright (c) 2008 Ahmed S. Badran

	Licensed under the FreeBSD License (see LICENSE)

          Filename:  port_any.h
       Description:  A port that specifies that the system should pick a port.
           Created:  12/20/2008 06:50:19 PM PST
            Author:  Ahmed S. Badran (Ahmed B.), ahmed.badran@gmail.com

********************************************************************************
*******************************************************************************/
#ifndef  PORT_ANY_INC
#define  PORT_ANY_INC

#include "port_if.h"
namespace simple_comm
{

class port_any: public port_if
{
	public:
	port_any();
	u16 get() const;

	private:
	u16 port_num;
};

}
#endif   // ----- #ifndef PORT_ANY_INC  -----
