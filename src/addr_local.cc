/*******************************************************************************
********************************************************************************

        Copyright (c) 2008 Ahmed S. Badran

	Licensed under the FreeBSD License (see LICENSE)

          Filename:  addr_local.cc
       Description:  Implementation.
           Created:  12/26/2008 04:15:46 PM PST
            Author:  Ahmed S. Badran (Ahmed B.), ahmed.badran@gmail.com

********************************************************************************
*******************************************************************************/
#include "addr_local.h"
using namespace std;
using namespace simple_comm;

addr_local::addr_local ():
	name("localhost")
{
}

const string
addr_local::get () const
{
	return name;
}

addr_if*
addr_local::clone () const
{
	return new addr_local;
}

