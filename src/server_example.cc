/*******************************************************************************
********************************************************************************

        Copyright (c) 2008 Ahmed S. Badran

	Licensed under the FreeBSD License (see LICENSE)

          Filename:  server_example.cc
       Description:  Sample server example.
           Created:  12/26/2008 07:54:39 PM PST
            Author:  Ahmed S. Badran (Ahmed B.), ahmed.badran@gmail.com

********************************************************************************
*******************************************************************************/
#include "server.h"
#include "port.h"
#include <cstring>
#include <cstdio>
using namespace simple_comm;

int main()
{
	port p(10000);
	server s(p);
	conn c = s.wait_for_connection();
	u32 i = 0;
	const u32 MAX = 100;
	u8 buf[MAX];
	printf("connection id: %d\n", c.get_id());
	while (i < 10) {
		memset(buf, '\0', MAX * sizeof(char));
		s.read(c, buf, MAX);
		printf("message %2d: %s\n", i + 1, buf);
		++i;
	}
	return 0;
}
