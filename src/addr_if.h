/*******************************************************************************
********************************************************************************

        Copyright (c) 2008 Ahmed S. Badran

	Licensed under the FreeBSD License (see LICENSE)

          Filename:  addr_if.h
       Description:  A network address interface.
           Created:  12/26/2008 03:41:18 PM PST
            Author:  Ahmed S. Badran (Ahmed B.), ahmed.badran@gmail.com

********************************************************************************
*******************************************************************************/
#ifndef  ADDR_IF_INC
#define  ADDR_IF_INC
#include <string>
namespace simple_comm
{

class addr_if
{
	public:
	virtual const std::string get() const = 0;
	virtual addr_if* clone() const = 0;
	~addr_if()
	{
	};
	private:
};

}
#endif   // ----- #ifndef ADDR_IF_INC  -----
