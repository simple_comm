/*******************************************************************************
********************************************************************************

        Copyright (c) 2008 Ahmed S. Badran

	Licensed under the FreeBSD License (see LICENSE)

          Filename:  sock_inf.h
       Description:  Socket information structure.
           Created:  12/20/2008 07:10:19 PM PST
            Author:  Ahmed S. Badran (Ahmed B.), ahmed.badran@gmail.com

********************************************************************************
*******************************************************************************/
#ifndef  SOCK_INF_INC
#define  SOCK_INF_INC
#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>
#include <cerrno>
namespace simple_comm
{

struct sock_inf {
	sock_inf():
		fd(0), addr_inf(NULL)
	{
	}
	int fd;
	struct addrinfo* addr_inf;
};

}
#endif   // ----- #ifndef SOCK_INF_INC  -----
