/*******************************************************************************
********************************************************************************

        Copyright (c) 2008 Ahmed S. Badran

	Licensed under the FreeBSD License (see LICENSE)

          Filename:  conn.h
       Description:  A connection end point.
           Created:  12/20/2008 08:07:34 PM PST
            Author:  Ahmed S. Badran (Ahmed B.), ahmed.badran@gmail.com

********************************************************************************
*******************************************************************************/
#ifndef  CONN_INC
#define  CONN_INC
namespace simple_comm
{
class conn
{
	public:
	explicit conn(int n);
	int get_id() const;
	~conn();
	private:
	int id;
};
}
#endif   // ----- #ifndef CONN_INC  -----
