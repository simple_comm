/*******************************************************************************
********************************************************************************

        Copyright (c) 2008 Ahmed S. Badran

	Licensed under the FreeBSD License (see LICENSE)

          Filename:  port_if.h
       Description:  A port interface.
           Created:  12/20/2008 05:06:24 PM PST
            Author:  Ahmed S. Badran (Ahmed B.), ahmed.badran@gmail.com

********************************************************************************
*******************************************************************************/
#ifndef  PORT_IF_INC
#define  PORT_IF_INC
#include "config.h"
namespace simple_comm
{

class port_if
{
	public:
	virtual ~port_if()
	{
	}
	virtual u16 get() const = 0;
	private:
};
}
#endif   // ----- #ifndef PORT_IF_INC  -----
