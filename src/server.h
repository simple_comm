/*******************************************************************************
********************************************************************************

        Copyright (c) 2008 Ahmed S. Badran

	Licensed under the FreeBSD License (see LICENSE)

          Filename:  server.h
       Description:  A simple server implementation.
           Created:  12/20/2008 03:23:48 PM PST
            Author:  Ahmed S. Badran (Ahmed B.), ahmed.badran@gmail.com

********************************************************************************
*******************************************************************************/
#ifndef  SERVER_INC
#define  SERVER_INC
#include "config.h"
#include "conn.h"
namespace simple_comm
{

class port_if;
struct sock_inf;

class server
{
	public:
	server(const port_if& p);
	conn wait_for_connection() const;
	u32 read (const conn&, u8* buf, u32 len) const;
	u32 write(const conn&, u8* buf, u32 len);
	~server();

	private:
	sock_inf* sock_info;
	private:
	server(const server&);
	const server& operator=(const server&);
};

}
#endif   // ----- #ifndef SERVER_INC  -----
