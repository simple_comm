/*******************************************************************************
********************************************************************************

        Copyright (c) 2008 Ahmed S. Badran

	Licensed under the FreeBSD License (see LICENSE)

          Filename:  port_any.cc
       Description:  Implementation.
           Created:  12/20/2008 06:56:01 PM PST
            Author:  Ahmed S. Badran (Ahmed B.), ahmed.badran@gmail.com

********************************************************************************
*******************************************************************************/
#include "port_any.h"
#include <cstdio>
using namespace simple_comm;

port_any::port_any ():
	port_num(0)
{
}

u16
port_any::get () const
{
	return port_num;
}

