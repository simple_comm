/*******************************************************************************
********************************************************************************

        Copyright (c) 2008 Ahmed S. Badran

	Licensed under the FreeBSD License (see LICENSE)

          Filename:  server.cc
       Description:  Implementation.
           Created:  12/20/2008 03:49:12 PM PST
            Author:  Ahmed S. Badran (Ahmed B.), ahmed.badran@gmail.com

********************************************************************************
*******************************************************************************/
#include "server.h"
#include "port_if.h"
#include "generic_exception.h"
#include "sock_inf.h"
#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>
#include <cerrno>
#include <cstring>

using namespace simple_comm;

server::server (const port_if& p):
	sock_info(0)
{
	sock_info = new sock_inf;
	sock_info->fd = socket(AF_INET, SOCK_STREAM, 0);
	if (sock_info->fd == -1) {
		throw generic_exception(strerror(errno));
	}
	struct addrinfo hint;
	memset(&hint, '\0', sizeof(hint));
	hint.ai_flags = AI_PASSIVE;
	hint.ai_family = AF_INET;
	hint.ai_socktype = SOCK_STREAM;
	char buf[10] = {0};
	sprintf(buf, "%d", p.get());
	int err = getaddrinfo("localhost", buf, &hint,
			      &(sock_info->addr_inf));
	if (err != 0) {
		throw generic_exception(gai_strerror(err));
	}
	if (bind(sock_info->fd, (struct sockaddr*)
		 sock_info->addr_inf->ai_addr,
		 sock_info->addr_inf->ai_addrlen) == -1) {
		throw generic_exception(strerror(errno));
	}

	if (listen(sock_info->fd, 1) == -1) {
		throw generic_exception(strerror(errno));
	}
}

conn
server::wait_for_connection () const
{
	int r = accept(sock_info->fd, (struct sockaddr*)
		       sock_info->addr_inf->ai_addr,
		       (socklen_t*) &(sock_info->addr_inf->ai_addrlen));
	if (r == -1) {
		throw generic_exception(strerror(errno));
	} else {
		return conn(r);
	}
}


u32
server::read (const conn& con, u8* buf, u32 len) const
{
	return ::read(con.get_id(), buf, len);
}

u32
server::write (const conn& con, u8* buf, u32 len)
{
	return ::write(con.get_id(), buf, len);
}

server::~server ()
{
	close(sock_info->fd);
	freeaddrinfo(sock_info->addr_inf);
}

