/*******************************************************************************
********************************************************************************

        Copyright (c) 2008 Ahmed S. Badran

	Licensed under the FreeBSD License (see LICENSE)

          Filename:  addr_ip.h
       Description:  An IP address.
           Created:  12/26/2008 04:21:24 PM PST
            Author:  Ahmed S. Badran (Ahmed B.), ahmed.badran@gmail.com

********************************************************************************
*******************************************************************************/
#ifndef  ADDR_IP_INC
#define  ADDR_IP_INC
#include "addr_if.h"
#include "config.h"

namespace simple_comm
{
class addr_ip: public addr_if
{
	public:
	addr_ip(u8, u8, u8, u8);
	const std::string get() const;
	addr_if* clone() const;
	private:
	u8 a, b, c, d;
	std::string name;
};

}
#endif   // ----- #ifndef ADDR_IP_INC  -----
